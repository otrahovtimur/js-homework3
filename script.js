/* ТЕОРІЯ

1. Логічні оператори - це оператори, які дозволяють порівнювати змінні та виконувати певні дії на основі результату цього порівняння.*/

/*2.  JavaScript є такі логічні оператори:
! (НЕ)
|| (АБО)
&& (І)
?? (Оператор об'єднання з нульовим заляганням)*/

//ПРАКТИКА

//1
let age = prompt('Вкажіть, будь ласка, Ваш вік!');

while (age != Number(age)) {
    age = prompt('Вкажіть, будь ласка, Ваш вік!')
}
if (age>0 && age<12) {
    alert('Ви ще дитина');
} else if (age>=12 && age<18) {
    alert('Ви ще підліток');
} else if (age>=18) {
    alert('Ви вже дорослий');
} else {
    alert('Ви ввели не існуючий вік')
}
console.log(age);

//2
let month = prompt("Введіть ,будь ласка ,місяць").toLowerCase();

switch(month){

    case "січень":
        console.log(31);
        break;

    case "лютий":
        console.log(28);
        break;   
        
    case "березень":
        console.log(31);
        break;

    case "квітень":
        console.log(30);
        break;

    case "травень":
        console.log(31);
        break;

    case "червень":
        console.log(30);
        break;
        
    case "липень":
        console.log(31);
        break;

    case "серпень":
        console.log(31);
        break;

    case "вересень":
        console.log(30);
        break;

    case "жовтень":
        console.log(31);
        break;

    case "листопад":
        console.log(30);
        break;

    case "грудень":
        console.log(31);
        break;

    default: 
    console.log("Error");
}

